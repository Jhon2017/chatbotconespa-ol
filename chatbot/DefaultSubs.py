"""This file contains the default (English) substitutions for the
PyAIML kernel.  These substitutions may be overridden by using the
Kernel.loadSubs(filename) method.  The filename specified should refer
to a Windows-style INI file with the following format:

    # lines that start with '#' are comments

    # The 'gender' section contains the substitutions performed by the
    # <gender> AIML tag, which swaps masculine and feminine pronouns.
    [gender]
    he = she
    she = he
    # and so on...

    # The 'person' section contains the substitutions performed by the
    # <person> AIML tag, which swaps 1st and 2nd person pronouns.
    [person]
    I = you
    you = I
    # and so on...

    # The 'person2' section contains the substitutions performed by
    # the <person2> AIML tag, which swaps 1st and 3nd person pronouns.
    [person2]
    I = he
    he = I
    # and so on...

    # the 'normal' section contains subtitutions run on every input
    # string passed into Kernel.respond().  It's mainly used to
    # correct common misspellings, and to convert contractions
    # ("WHAT'S") into a format that will match an AIML pattern ("WHAT
    # IS").
    [normal]
    what's = what is
"""

defaultGender = {
    # masculine -> feminine
    "él": "ella",
    # feminine -> masculine    
    "ella": "él",
}

defaultPerson = {
    # 1st->3rd (masculine)
    "yo": "él",
    "mi": "su",
    "mio": "suyo",

    # 3rd->1st (masculine)
    "él": "yo",
    "su": "mi",
    "él mismo": "yo mismo",
    
    # 3rd->1st (feminine)
    "ella": "yo",
    "suyo": "mío",
    "ella misma": "yo misma",
}

defaultPerson2 = {
    # 1st -> 2nd
    "yo": "Tú",
    "mi": "tu",
    "Mío": "tuyo",
    "Yo mismo": "Tú mismo",

    # 2nd -> 1st
    "Tú": "yo",
    "tu": "mi",
    "tuyo": "Mío",
}


# TODO: this list is far from complete
defaultNormal = {
    "sip": "Sí",
    "afirmativo": "Sí",
    "Recibido": "Sí",
    "Seguro": "Sí",
    "sí señor": "Sí",
    "Negativo": "No",
    "ninguna": "No",
    "ok": "Está bien",
    "De acuerdo": "Está bien",
    "okey": "Está bien",
}
