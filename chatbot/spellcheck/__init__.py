from os import path
WORDS = {}
'''genera el diccionario de palabra con su clave y valor obtenidas de
https://onedrive.live.com/?id=3732E80B128D016F%213584&cid=3732E80B128D016F
son las palabras y la frecuencia en que se usan.
'''
# es = open('es.txt')
es = open(path.join(path.dirname(path.abspath(__file__)),'es.txt'),encoding='utf-8')

for i in es:
    w = i.split(" ")
    WORDS[w[0]] = int(w[1])
#probabilidad de una palabra
def P(word, N=sum(WORDS.values())): 
    "Probability of `word`."
    if WORDS.get(word) is None:
        return 0
    return WORDS.get(word)/N
#corrige la palabra segun la probabilidad de una palabra
def correction(word): 
    "Most probable spelling correction for word."
    return max(candidates(word), key=P)
#palabra canditas con las posibles ediciones 
def candidates(word): 
    "Generate possible spelling corrections for word."
    return (known([word]) or known(edits1(word)) or known(edits2(word)) or [word])
#revisa el conjunto de palabras conocidas creando un conjunto de la palabra si es que la contiene
#sino esta el conjunto es vacio
def known(words): 
    "The subset of `words` that appear in the dictionary of WORDS."
    return set(w for w in words if w in WORDS)
# La función edits1 devuelve un conjunto de todas las cadenas editadas (ya sean palabras o no) que se pueden hacer con una simple edición:
def edits1(word):
    "All edits that are one edit away from `word`."
    letters    = 'abcdefghijklmnopqrstuvwxyz'
    splits     = [(word[:i], word[i:])    for i in range(len(word) + 1)]
    deletes    = [L + R[1:]               for L, R in splits if R]
    transposes = [L + R[1] + R[0] + R[2:] for L, R in splits if len(R)>1]
    replaces   = [L + c + R[1:]           for L, R in splits if R for c in letters]
    inserts    = [L + c + R               for L, R in splits for c in letters]
    return set(deletes + transposes + replaces + inserts)

def edits2(word): 
    "All edits that are two edits away from `word`."
    return (e2 for e1 in edits1(word) for e2 in edits1(e1))
