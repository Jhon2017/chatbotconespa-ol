#!/usr/bin/env python
#from distutils.core import setup
from setuptools import setup

setup(
    name = 'chatbotGreenit',
    version='0.2.0.8',
    author = "Infoverde",
    author_email = "infoverdedocumentacion@gmail.com",
    url="https://gitlab.com/Jhon2017/chatbotconespa-ol",
    description="Chat bot de consejos de green it",
    long_description=open("README.rst").read(),
    packages=['chatbot','chatbot.spellcheck'],
    license='MIT',
    keywords='chatbot green it engine and chat builder platform',
    platforms=["Windows", "Linux", "Solaris", "Mac OS-X", "Unix"],
    package_dir={ 'chatbot': 'chatbot','chatbot.spellcheck':'chatbot/spellcheck'},
    include_package_data = True,
    package_data   = { "chatbot":  ["default.template","spellcheck/es.txt"]},
    install_requires=[
          'requests',
      ]
)
